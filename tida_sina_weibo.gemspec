# -*- encoding: utf-8 -*-
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'tida_sina_weibo/version'

Gem::Specification.new do |gem|
  gem.name          = "tida_sina_weibo"
  gem.version       = TidaSinaWeibo::VERSION
  gem.authors       = ["babaru"]
  gem.email         = ["babaru.trinit@gmail.com"]
  gem.description   = %q{SinaWeibo API gem}
  gem.summary       = %q{SinaWeibo API gem}
  gem.homepage      = ""

  gem.files         = `git ls-files`.split($/)
  gem.executables   = gem.files.grep(%r{^bin/}).map{ |f| File.basename(f) }
  gem.test_files    = gem.files.grep(%r{^(test|spec|features)/})
  gem.require_paths = ["lib"]

  gem.add_development_dependency "rspec", "~> 2.6"
  gem.add_dependency "rest-client"
end
