require 'tida_sina_weibo'

describe TidaSinaWeibo::ApiWrapper do
  it 'get setting property' do
    # ApiSettings.should eql('https://api.weibo.com/2/statuses/update.json')
  end

  it 'get request success' do
    request_wrapper = double('request_wrapper')
    request_wrapper.stub(:get) {{'result' => 'ok'}}

    token_provider = double('token_provider')
    token_provider.stub(:token) { 'token'}

    target = TidaSinaWeibo::ApiWrapper.new token_provider, request_wrapper
    result = target.status_show '1'
    result.should eql({'result' =>'ok'})
  end

  it 'get request raise invalid_access_token exception' do
    rest_client_exception = TidaSinaWeibo::RestClientException.new nil

    request_wrapper = double('request_wrapper')
    request_wrapper.should_receive(:get).and_raise(rest_client_exception)

    rest_client_exception.stub(:body) { {'error_code' => '21332'} }

    token_provider = double('token_provider')
    token_provider.stub(:token) { 'token' }
    token_provider.stub(:change)
    token_provider.stub(:token) { nil }

    target = TidaSinaWeibo::ApiWrapper.new token_provider, request_wrapper
    begin
      result = target.status_show '1'
      raise 'Expect exception raise'
    rescue TidaSinaWeibo::NoAccessTokenAvaiableException
    end
  end

  it 'get request raise reached_account_access_limit exception' do
    rest_client_exception = TidaSinaWeibo::RestClientException.new nil

    request_wrapper = double('request_wrapper')
    request_wrapper.should_receive(:get).and_raise(rest_client_exception)

    rest_client_exception.stub(:body) { {'error_code' => '10023'} }

    token_provider = double('token_provider')
    token_provider.stub(:token) { 'token' }
    token_provider.stub(:change)
    token_provider.stub(:token) { nil }

    target = TidaSinaWeibo::ApiWrapper.new token_provider, request_wrapper
    begin
      result = target.status_show '1'
      raise 'Expect exception raise'
    rescue TidaSinaWeibo::NoAccessTokenAvaiableException
    end
  end
end