
require "tida_sina_weibo/version"
require 'tida_sina_weibo/exceptions'
require 'tida_sina_weibo/api_wrapper'
require 'tida_sina_weibo/rest_client_wrapper'

module TidaSinaWeibo

  def api_wrapper(tp)
    ApiWrapper.new RestClientWrapper.new, tp
  end

end

