module TidaSinaWeibo
  module Generators
    class InstallGenerator < Rails::Generators::Base
      source_root File.expand_path("../../templates", __FILE__)

      def copy_settings
        copy_file "../../../config/tida_sina_weibo.yml", "config/tida_sina_weibo.yml"
      end
    end
  end
end