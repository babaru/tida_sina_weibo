require "rest-client"
require 'tida_sina_weibo/api_settings'

module TidaSinaWeibo

  class AutoTokenSwitchApiWrapper

    attr_reader :access_token
    attr_reader :request_data
    attr_reader :token_provider
    attr_reader :rest_client
    
    def initialize(tp, rc = nil)
      rc = RestClientWrapper.new if rc.nil?
      @rest_client = rc
      @token_provider = tp
      @access_token = tp.token
    end
    
    def status_update(content)
      post_request(ApiSettings.api.status.update, :status => content)
    end
    
    def status_upload(content, image_path)
      post_request(ApiSettings.api.status.upload, :status => content, :pic => File.new(image_path, 'rb'))
    end
    
    def status_user_timeline(uid, page = 1, count = 100)
      get_request(ApiSettings.api.status.user_timeline, :params => {:uid => uid, :page => page, :count => count})
    end
    
    def status_repost_timeline(post_id, page = 1)
      get_request(ApiSettings.api.status.repost_timeline, :params => {:id => post_id, :page => page, :count => 50})
    end

    def comments_show(post_id, page = 1)
      get_request(ApiSettings.api.comments.show, :params => {:id => post_id, :page => page, :count => 50})
    end
    
    def status_show(wb_post_id)
      get_request(ApiSettings.api.status.show, :params => {:id => wb_post_id})
    end
    
    def user_show(uid, is_number_id = true)
      if is_number_id
        get_request(ApiSettings.api.user.show, {:params => {:uid => uid}})
      else
        get_request(ApiSettings.api.user.show, {:params => {:screen_name => uid}})
      end
    end

    def user_domain_show(domain)
      get_request(ApiSettings.api.user.domain_show, :params => {:domain => domain})
    end

    def friendships_followers(uid, cursor)
      get_request(ApiSettings.api.friendships.followers, :params => {:uid => uid, :cursor => cursor})
    end

    def friendships_followers_ids(uid, cursor)
      get_request(ApiSettings.api.friendships.followers_ids, :params => {:uid => uid, :cursor => cursor})
    end
    
    def tags(uid)
      get_request(ApiSettings.api.tags, :params => {:uid => uid})
    end
    
    def queryid(mid)
      get_request(ApiSettings.api.status.queryid, {:params => {:mid => mid, :type => 1, :isBase62 => 1}})
    end

    def querymid(id, batch = false)
      get_request(ApiSettings.api.status.querymid, {:params => {:id => id, :type => 1, :is_batch => batch ? 1 : 0}})
    end

    def querymids(ids)
      ids_str = ids.join(',')
      querymid(ids_str, true)
    end
    
    private
    
    def post_request(url, data)
      rest_request(url, data, true)
    end
    
    def get_request(url, data)
      rest_request(url, data, false)
    end

    def rest_request(url, data, method_post)
      result, code = do_request url, data, method_post

      while code == -1
        change_access_token
        result, code = do_request url, data, method_post
      end

      return result
    end
    
    def do_request(url, data, method_post)
      rd  = data.clone
      params = {:access_token => @access_token}
      rd[:params] = data[:params].merge(params)

      if method_post
        r = rest_client.post(url, rd)
        return r, 0
      else
        r = rest_client.get(url, rd)
        return r, 0
      end
    rescue RestClientException => e
      r = e.body
      api_error_code = r["error_code"].to_i

      if invalid_access_token? api_error_code
        return nil, -1
      end

      if reached_account_access_limit? api_error_code
        return nil, -1
      end

      if reached_ip_access_limit? api_error_code
        e = IPAccessLimitException.new self.access_token
        raise e
      end

      return nil, -2
    end

    def invalid_access_token?(error_code)
      return error_code == 21332 || error_code == 21327
    end

    def reached_account_access_limit?(error_code)
      return error_code == 10023 || error_code == 10024
    end

    def reached_ip_access_limit?(error_code)
      return error_code == 10022
    end

    def change_access_token
      self.token_provider.change
      @access_token = self.token_provider.token
      if @access_token.nil?
        e = NoAccessTokenAvaiableException.new
        raise e
      end
    end
  end
end