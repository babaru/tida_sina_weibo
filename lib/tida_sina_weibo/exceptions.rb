require 'rest-client'

module TidaSinaWeibo

  class Exception < RuntimeError
    attr_accessor :message

    def initialize message = nil
      @message = message
    end

    def inspect
      self.message
    end

    def to_s
      inspect
    end
  end

  class NoAccessTokenAvaiableException < Exception
    def initialize
      super "There is not any valid access token"
    end 
  end

  class IPAccessLimitException < Exception
    def initialize
      super "IP access limit reached"
    end
  end

  class InvalidAccessTokenException < Exception
    def initialize(token)
      super "The access token #{token} is not valid"
    end
  end

  class RestClientException < RestClient::Exception
    attr_reader :body

    def initialize(rest_client_exception)
      @body = ::JSON.parse rest_client_exception.http_body if rest_client_exception
    end
  end
  
end