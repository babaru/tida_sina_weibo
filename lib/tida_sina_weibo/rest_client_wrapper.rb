require 'rest-client'

module TidaSinaWeibo

  class RestClientWrapper
    def get(url, data)
      JSON.parse RestClient.get(url, data)
    rescue RestClient::Exception => e
      ex = RestClientException.new e
      raise ex
    end

    def post(url, data)
      JSON.parse RestClient.post(url, data)
    rescue RestClient::Exception => e
      ex = RestClientException.new e
      raise ex
    end
  end

end